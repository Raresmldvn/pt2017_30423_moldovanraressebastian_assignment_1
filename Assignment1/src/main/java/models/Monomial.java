package models;

/*
 * Abstract class that models the general monomial methods and concepts
 */
public abstract class Monomial {

	public Monomial(){}
	
	public abstract Monomial additionMonomial(Monomial secondMonomial);
	
	public abstract Monomial substractionMonomial(Monomial secondMonomial);
	
	public abstract Monomial multiplicationMonomial(Monomial secondMonomial);
	
	public abstract Monomial divisionMonomial(Monomial secondMonomial);
	
	public abstract int getPower();
	
	public abstract Number getCoefficient();
	
	public abstract String toString();
	
	public abstract Monomial minusMonomial();
	
	public abstract Monomial differentiateMonomial();
	
	public abstract Monomial integrateMonomial();
		
	}
	

