package models;

import java.util.Comparator;

public class MonomialComparator implements Comparator<Monomial> {


	public int compare(Monomial M1, Monomial M2){
		return M1.getPower() - M2.getPower();
	}
}
