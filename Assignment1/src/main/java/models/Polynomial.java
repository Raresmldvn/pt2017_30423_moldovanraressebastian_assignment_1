package models;

import java.util.*;
/*
 * Class that models the mathematical concept of polynomial
 * Polynomial is seen as a list of monomials.
 * Implements methods for accessing certain elements.  */
public class Polynomial {

	private ArrayList<Monomial> listOfMonomials;
	
	public Polynomial(ArrayList<Monomial> monomials) {
		this.listOfMonomials = monomials;
	}
	
	public void addMonomial(Monomial newMonomial) {
		this.listOfMonomials.add(newMonomial); //add a new monomial to the polynomial by ussing predefined add method for lists
	}
	
	public ArrayList<Monomial> getMonomials()
	{
		return listOfMonomials;
	}
	
	public Monomial getMonomialForPower(int power){
		
		//iterate polynomial list 
		for(Monomial currentMonomial: listOfMonomials){
			if(currentMonomial.getPower() == power){
				return currentMonomial; //if found a monomial with specified power, return it
			}
		}
		return new IntegerMonomial(0,0); //if no monomial is found ,return symbolically a 0 monomial 
	}
	
	public int getDegree() {
		
		return this.getLeadTerm().getPower(); //degree of a polynomial is the power of the lead term
		}
	
	public Monomial getLeadTerm() {
		
		Monomial leadTerm = new IntegerMonomial(0,0); //initialize lead term with 0-monomial
		
		if(listOfMonomials.size()==1 && listOfMonomials.get(0).getPower()==0) //if polynomial is made up only of a free term
			return listOfMonomials.get(0); //return free term
		//perform a maximum-search on the list of monomials, consider maximum monomial the monomial with the greatest power
		for(Monomial currentMonomial: listOfMonomials){
			if(currentMonomial.getPower() > leadTerm.getPower())
				leadTerm = currentMonomial; 
		}
		return leadTerm;
		
	}
}
	