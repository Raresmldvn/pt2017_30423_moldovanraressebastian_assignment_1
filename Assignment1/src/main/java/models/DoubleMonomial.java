package models;

import java.text.DecimalFormat;

public class DoubleMonomial extends Monomial {

	private Double coefficient;
	private int power;
	
	public DoubleMonomial(Double coef, int pow){
		
		super();
		this.coefficient= coef;
		this.power = pow;
	}
	
	public int getPower() {
		return power;
	}
	
	public Double getCoefficient() {
		
		return coefficient;
	}
	
	public Monomial additionMonomial(Monomial secondMonomial){
		//create new double monomial with characteristics: coefficient = sum of coefficifients of cuurent monomial and added monomial
		DoubleMonomial addition = new DoubleMonomial(new Double(this.coefficient.doubleValue()+secondMonomial.getCoefficient().doubleValue()),secondMonomial.getPower());
		return addition;
	}
	
	public Monomial substractionMonomial(Monomial secondMonomial){
		//create new double monomial with characteristics: coefficient = difference of coefficients of current monomial and added monomial
		DoubleMonomial addition = new DoubleMonomial(new Double(this.coefficient.doubleValue()-secondMonomial.getCoefficient().doubleValue()),secondMonomial.getPower());
		return addition;
	}

	
	public Monomial multiplicationMonomial(Monomial secondMonomial){
		//create new double monomial with characteristics: coefficient = product of coefficients(current and multiplied), power = sum of powers(current and multiplied) 
		DoubleMonomial multiplication = new DoubleMonomial( new Double(this.coefficient.doubleValue()*secondMonomial.getCoefficient().doubleValue()),this.power+secondMonomial.getPower());
		return multiplication;
	}
	
	public Monomial divisionMonomial(Monomial secondMonomial){
		//create new double monomial with characteristics: coefficient = quotient of coefficients(current and multiplied), power = difference of powers(current and multiplied) 
		DoubleMonomial division = new DoubleMonomial(new Double(this.coefficient.doubleValue()/secondMonomial.getCoefficient().doubleValue()),this.power - secondMonomial.getPower());
		return division;
	}
	
	
public Monomial differentiateMonomial(){
		//create new double monomial that obeys the rules of differentiation
		DoubleMonomial diff = new DoubleMonomial(new Double(this.coefficient.doubleValue()*this.power), this.power -1);
		return diff;
	}
	
	public Monomial integrateMonomial(){
		//create new double monomial that obeys the rules of integration
		DoubleMonomial intg = new DoubleMonomial(new Double(this.coefficient.doubleValue()/(this.power+1)), this.power+1);
		return intg;
	}
	
	public Monomial minusMonomial(){
		//create new integer monomial with opposite coefficient and same power
		DoubleMonomial minus = new DoubleMonomial(new Double((-1)*this.coefficient.doubleValue()),this.power);
		return minus;
	}
	
	public String toString(){
	String result = new String("");
	DecimalFormat twoDecimal = new DecimalFormat("0.##"); //define decimal format with maximum 2 decimals 
	double coef = this.coefficient.doubleValue();
	if(this.power==0){
		result = result + twoDecimal.format(coef); //display free term with no power indicator
	}
	else{
		if(coef==1){ 
			result = result + "+"; //display only a '+' for coefficient 1
		}
		else if(coef==-1){
			result = result + "-"; //display only a '-' for coefficient -1 
		}
		else{
			if(coef>0){
			result = result + "+"; //display a '+' for positive numbers, no additional characte for negative numbers since '-' is implied
			}
			result = result + twoDecimal.format(coef); //display coefficient
	}
	result = result + "x^" + Integer.toString(this.power); //display power

	}
	return result;
	}
}
