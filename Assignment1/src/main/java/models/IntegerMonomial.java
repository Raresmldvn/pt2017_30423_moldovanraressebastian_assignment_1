package models;

public class IntegerMonomial extends Monomial {

	private Integer coefficient;
	private int power;
	
	public IntegerMonomial(Integer coef, int pow){
		super();
		this.coefficient = coef;
		this.power = pow;
	}
	
	public int getPower() {
		return power;
	}
	
	public Integer getCoefficient() {
		
		return coefficient;
	}
	
	public Monomial additionMonomial(Monomial secondMonomial){
		//create new integer monomial with characteristics: coefficient = sum of coefficifients of cuurent monomial and added monomial
		IntegerMonomial addition = new IntegerMonomial(new Integer(this.coefficient.intValue()+secondMonomial.getCoefficient().intValue()), secondMonomial.getPower());
		return addition;
	}
	
	public Monomial substractionMonomial(Monomial secondMonomial){
		
		//create new integer monomial with characteristics: coefficient = difference of coefficients of current monomial and subtracted monomial
		IntegerMonomial substraction = new IntegerMonomial(new Integer(this.coefficient.intValue()-secondMonomial.getCoefficient().intValue()),secondMonomial.getPower());
	    return substraction;
	}
	
	public Monomial multiplicationMonomial(Monomial secondMonomial){
		
		//create new integer monomial with characteristics: coefficient = product of coefficients(current and multiplied), power = sum of powers(current and multiplied) 
		IntegerMonomial multiplication = new IntegerMonomial(new Integer(this.coefficient.intValue()*secondMonomial.getCoefficient().intValue()),this.power+secondMonomial.getPower());
		return multiplication;
	}
	
	public Monomial divisionMonomial(Monomial secondMonomial){
		//create new double monomial because the division of coefficients might output a double value, new power is subraction of powers
		DoubleMonomial result = new DoubleMonomial(new Double(this.coefficient.doubleValue()/secondMonomial.getCoefficient().doubleValue()),
				                this.power - secondMonomial.getPower());
		return result;
	}
	
	public Monomial differentiateMonomial(){
		
		//create new integer monomial by obeying rule of differentiation: coefficient becomes coefficient times power, and power decremented
		IntegerMonomial diff = new IntegerMonomial(new Integer(this.coefficient.intValue()*this.power), this.power -1);
		return diff;
	}
	
	public Monomial integrateMonomial(){
		//create new double monomial by obeying rule of integration: coefficient becomes coefficient over power incremented, power incremented 
		DoubleMonomial intg = new DoubleMonomial(new Double(this.coefficient.doubleValue()/(this.power+1)), this.power+1);
		return intg;
	}
	
	public Monomial minusMonomial(){
		//create new integer monomial with opposite coefficient and same power
		IntegerMonomial minus = new IntegerMonomial(new Integer((-1)*this.coefficient), this.power);
		return minus;
	}
	
	public String toString(){
		
		String result = new String("");  //initialize resulting string 
		int coef = this.coefficient.intValue(); //compute coefficient
		if(this.power==0){ //if lead term,  represent only coefficient 
			if(coef>0) 
				result = result + "+";
			result = result + Integer.toString(coef);
		}
		else{ //if not lead term
			if(coef==1) //if coefficient is 1, display only a '+'
				result = result + "+";
			else if(coef==-1) //if coefficient is -1, display only a '-'
				result = result + "-";
			else{
				if(coef>0) //if coefficient is positive, force a "+" 
					result = result + "+";
					result = result + Integer.toString(coef);
			}
			//display power 
			result = result + "x^" + Integer.toString(this.power);

		}
		return result;
		
	}
	
}
