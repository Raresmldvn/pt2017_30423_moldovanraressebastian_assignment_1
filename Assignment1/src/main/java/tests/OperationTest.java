package tests;
import application.Operation;
import models.Polynomial;
import static org.junit.Assert.*;

import org.junit.Test;

public class OperationTest {

	@Test
	public void operationsTest() {
		
		//Test for random polynomials, constructions toghether with representations and operations
		Operation O = new Operation();
		Polynomial P1 = O.constructPolynomial("1+x^2");
		Polynomial P2 = O.constructPolynomial("1+3x^3+x^5");
		String result = O.representPolynomial(O.addPolynomials(P1, P2));
		assertEquals(result,"2+x^2+3x^3+x^5");
		result = O.representPolynomial(O.substractPolynomials(P1, P2));
		assertEquals(result,"x^2-3x^3-x^5");
		result = O.representPolynomial(O.multiplyPolynomials(P1, P2));
		assertEquals(result, "1+x^2+3x^3+4x^5+x^7");
		result = O.representPolynomial(O.differentiatiePolynomial(P2));
		assertEquals(result, "9x^2+5x^4");
		result = O.representPolynomial(O.integratePolynomial(P2));
		assertEquals(result, "x^1+0.75x^4+0.17x^6");
		
		P1 = O.constructPolynomial("x^3-2x^2-4");
		P2 = O.constructPolynomial("x^1-3");
		Polynomial[] results;
		results = O.dividePolynomials(P1, P2);
		assertEquals(O.representPolynomial(results[0]),"3+x^1+x^2");
		assertEquals(O.representPolynomial(results[1]),"5");
		
		//Test for simple numbers
		P1 = O.constructPolynomial("3");
		P2 = O.constructPolynomial("2");
		result = O.representPolynomial(O.addPolynomials(P1,P2));
		assertEquals(result,"5");
		result = O.representPolynomial(O.multiplyPolynomials(P1, P2));
		assertEquals(result,"6");
		results = O.dividePolynomials(P1, P2);
		assertEquals(O.representPolynomial(results[0]),"1.5");
		assertEquals(O.representPolynomial(results[1]),"0");
		
		//Test for 0 polynomials
		P1 = O.constructPolynomial("0");
		P2 = O.constructPolynomial("0");
		result = O.representPolynomial(O.addPolynomials(P1, P2));
		assertEquals(result, "0");
		result = O.representPolynomial(O.substractPolynomials(P1, P2));
		assertEquals(result,"0");
		
	}

}


