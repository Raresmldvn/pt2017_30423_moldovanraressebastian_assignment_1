package application;

import java.util.*;

import models.Polynomial;
import models.MonomialComparator;
import models.Monomial;
import models.IntegerMonomial;


public class Operation {

	public Polynomial addPolynomials(Polynomial firstPolynomial, Polynomial secondPolynomial) {
		
		Polynomial resultingPolynomial = new Polynomial(new ArrayList<Monomial>());
		//add monomials of the first polynomial in resulting polynomial
		for(Monomial currentMonomial: firstPolynomial.getMonomials()){
			resultingPolynomial.addMonomial(currentMonomial);
		}
		//iterate second polynomial
		for(Monomial currentMonomial: secondPolynomial.getMonomials()){
			int currentPower = currentMonomial.getPower();
			if(resultingPolynomial.getMonomialForPower(currentPower).getCoefficient().intValue()==0) //check if monomial with power exists
				resultingPolynomial.addMonomial(currentMonomial); //if not, add monomial in resulting polynomial
			else{
				//check if monomials do not cancel
				if((resultingPolynomial.getMonomialForPower(currentPower).getCoefficient().doubleValue()+ currentMonomial.getCoefficient().doubleValue())!=0){ 
				//add sum of monomials in current polynomial
				resultingPolynomial.addMonomial(currentMonomial.additionMonomial(resultingPolynomial.getMonomialForPower(currentPower)));
				}
				//remove previous value of monomial of power detected
			    resultingPolynomial.getMonomials().remove(resultingPolynomial.getMonomialForPower(currentPower));
			}
		}
		Collections.sort(resultingPolynomial.getMonomials(), Collections.reverseOrder(new MonomialComparator())); //sort monomials
		
		return resultingPolynomial;
	}

public Polynomial substractPolynomials(Polynomial firstPolynomial, Polynomial secondPolynomial) {
		
		Polynomial resultingPolynomial = new Polynomial(new ArrayList<Monomial>());
		for(Monomial currentMonomial: firstPolynomial.getMonomials()){
			resultingPolynomial.addMonomial(currentMonomial); //add monomials of the first polynomial in the resulting polynomial
		}
		//iterate second polynomial 
		for(Monomial currentMonomial: secondPolynomial.getMonomials()){
			int currentPower = currentMonomial.getPower();  
			if(resultingPolynomial.getMonomialForPower(currentPower).getCoefficient().intValue()==0) //check if a monomial with this power does not exist
				resultingPolynomial.addMonomial(currentMonomial.minusMonomial()); //add the opposite monomial to the result
			else{
				if((resultingPolynomial.getMonomialForPower(currentPower).getCoefficient().doubleValue()-currentMonomial.getCoefficient().doubleValue())!=0){
					//if terms do not cancel, add the result of the subtraction in the final polynomial
					Monomial auxiliaryMonomial = resultingPolynomial.getMonomialForPower(currentPower);
					resultingPolynomial.addMonomial(auxiliaryMonomial.substractionMonomial(currentMonomial));
		
				}
				//either if they canceled or not, the previous existing monomial is removed
			    resultingPolynomial.getMonomials().remove(resultingPolynomial.getMonomialForPower(currentPower));
			}
		}
		Collections.sort(resultingPolynomial.getMonomials(), Collections.reverseOrder(new MonomialComparator())); //sort monomials in the list
		
		return resultingPolynomial;
	}
	

	public Polynomial multiplyPolynomials(Polynomial firstPolynomial, Polynomial secondPolynomial){
		Polynomial resultingPolynomial = new Polynomial(new ArrayList<Monomial>()); //initialize resulting polynomial  
		Polynomial singleMonomial; //this is a single monomial - type polynomial
		Operation O = new Operation();
		//get all pairs of monomials with the property that first element belongs to first polynomial and second element to second polynomial
		for(Monomial currentFirst: firstPolynomial.getMonomials()){
			for(Monomial currentSecond: secondPolynomial.getMonomials()){
				singleMonomial = new Polynomial(new ArrayList<Monomial>()); //reinitialize every time the single monomial type polynomial 
				singleMonomial.addMonomial(currentFirst.multiplicationMonomial(currentSecond)); //compute multiplication of monomials and add result to auxiliary
				resultingPolynomial = O.addPolynomials(resultingPolynomial, singleMonomial); //compute addition between current result and computed auxiliary
			}
		}
		return resultingPolynomial;
	}
	
	public Polynomial[] dividePolynomials(Polynomial firstPolynomial, Polynomial secondPolynomial){
		
		Polynomial[] resultingPolynomials = new Polynomial[2];  //we will place the result in a 2-element string vector
		Polynomial quotientPolynomial = new Polynomial(new ArrayList<Monomial>()); //quotient polynomial is initialized
		Polynomial remainderPolynomial = firstPolynomial; //remainder polynomial is initialized with dividend 
		Polynomial auxiliaryPolynomial1 = new Polynomial(new ArrayList<Monomial>());
		Polynomial auxiliaryPolynomial2 = new Polynomial(new ArrayList<Monomial>());
		Operation O = new Operation();
		
		while((remainderPolynomial.getMonomials().isEmpty()==false) && (remainderPolynomial.getDegree() >= secondPolynomial.getDegree())){
			
			auxiliaryPolynomial1.getMonomials().clear(); //reinitialize first auxiliary at every step
			//compute in auxiliary the division between the lead terms of remainder and divisor
			auxiliaryPolynomial1.addMonomial(remainderPolynomial.getLeadTerm().divisionMonomial(secondPolynomial.getLeadTerm()));
			quotientPolynomial = O.addPolynomials(quotientPolynomial, auxiliaryPolynomial1); //add result in quotient
			auxiliaryPolynomial2 = O.multiplyPolynomials(auxiliaryPolynomial1, secondPolynomial); //compute product between first auxiliary and divisor 
			remainderPolynomial = O.substractPolynomials(remainderPolynomial, auxiliaryPolynomial2); //subtract result from remainder
		}
		resultingPolynomials[0] = quotientPolynomial; 
		resultingPolynomials[1] = remainderPolynomial;
		return resultingPolynomials;
	}
	
	public Polynomial differentiatiePolynomial(Polynomial myPolynomial){
		
		Polynomial resultingPolynomial = new Polynomial(new ArrayList<Monomial>());
		//the list is iterated and every monomial is replaced with the corresponding result for differentiation 
		for(Monomial currentMonomial: myPolynomial.getMonomials()){
			if(currentMonomial.getPower()!=0){
				resultingPolynomial.addMonomial(currentMonomial.differentiateMonomial());
			}
		}
		return resultingPolynomial;
	
	}
	
	public Polynomial integratePolynomial(Polynomial myPolynomial){
		
		Polynomial resultingPolynomial = new Polynomial(new ArrayList<Monomial>());
		//the list is iterated and every monomial is replaced with the corresponding result for integration
		for(Monomial currentMonomial: myPolynomial.getMonomials())
				resultingPolynomial.addMonomial(currentMonomial.integrateMonomial());
		
		return resultingPolynomial;
	}
	
	
	public String representPolynomial(Polynomial myPolynomial){
		
		String representation = new String("");
		if(myPolynomial.getMonomials().size()==0) //the representation of the empty polynomial is "0" 
			return new String("0");
		//iterate list of monomials and represent each
		for(Monomial currentMonomial: myPolynomial.getMonomials()){
			
			representation = representation + currentMonomial.toString();
		}
		//if the representation happens to begin with an unnceseary "+", due to the presence of the lead term on the first position, delete it
		if(representation.charAt(0)=='+')
			representation = representation.substring(1);
		return representation;
	}
	
	public Polynomial constructPolynomial(String input){
		
		if(input.charAt(0)=='0')
			return new Polynomial(new ArrayList<Monomial>()); //if input is 0, return empty polynomial 
		Polynomial resultingPolynomial = new Polynomial(new ArrayList<Monomial>());
		String[] inputs = input.split("[x]|(?=\\+)|(?=\\-)"); //split string into string vector of type coefficient or power
		for(int i=0;i<inputs.length;i++){
			if(inputs[i].length()==0||("0123456789".contains(Character.toString(inputs[i].charAt(0)))==false&&inputs[i].length()==1)){
				//being here means we found a non-numeric character or string begins directly with an 'x' or '-x'
				if(inputs[i].length()==0 || inputs[i].charAt(0)=='+')
					resultingPolynomial.addMonomial(new IntegerMonomial(new Integer(1),Integer.parseInt(inputs[i+1].substring(1))));
				else if(inputs[i].charAt(0)=='-')
					resultingPolynomial.addMonomial(new IntegerMonomial(new Integer(-1),Integer.parseInt(inputs[i+1].substring(1))));
				i++;
			}
			else{
				if((i+1)<inputs.length){ //check if we can reffer to a next term, or term is last and we reached this because last term is free term
					if(inputs[i+1].charAt(0) == '^'){ //best case, we are given both coefficient and power explicitly and we add them to polynomial
						resultingPolynomial.addMonomial(new IntegerMonomial(Integer.parseInt(inputs[i]),Integer.parseInt(inputs[i+1].substring(1))));
						i++;
					}
					else
						resultingPolynomial.addMonomial(new IntegerMonomial(Integer.parseInt(inputs[i]),0)); //no power given means free term
					
					
				}else
					resultingPolynomial.addMonomial(new IntegerMonomial(Integer.parseInt(inputs[i]),0)); //we are in last position and have no power, then free term
			}
		}
		return resultingPolynomial;
	}
	
	}