package application;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
/*
 * Class that implements the user interface using the swing libraries.*/
public class View extends JFrame {

	private String[] algebraicOperations = {"ADD","SUBSTRACT","MULTIPLY","DIVIDE","DIFFERENTIATIE", "INTEGRATE"};
	private JTextField firstTextField = new JTextField(30);
	private JTextField secondTextField = new JTextField(30);
	private JTextField thirdTextField = new JTextField(30);
	private JComboBox<String> comboBox = new JComboBox<String>(algebraicOperations);
	private JButton computeButton = new JButton("Compute");
	private Operation operation;
	
	public View(Operation op){
		
		this.operation = op;
		//first panel is the Title panel, 
		JPanel panel0 = new JPanel();
		JLabel label0 = new JLabel("POLYNOMIAL CALCULATOR"); //set title in label 
		panel0.add(label0); //add label to panel 
		label0.setFont(new Font("Serif", Font.PLAIN, 30));
		panel0.setPreferredSize(new Dimension(500,0));
		panel0.setBackground(Color.white);
		
		//panel 1 contans the 2 text field necessary for input
		JPanel panel1 = new JPanel();
		panel1.setLayout(new FlowLayout()); //set layout to type flow layout
		JLabel label1 = new JLabel("Polynomial 1"); // label1 indicates that user inputs first polynomial
		label1.setFont(new Font("Serif", Font.PLAIN, 20));
		panel1.add(label1);
		firstTextField.setPreferredSize(new Dimension(200,30));
		panel1.add(firstTextField); //add first text field to panel
		JLabel label2 = new JLabel("Polynomial 2"); //label 2 indicated that user inputs second polynomial 
		label2.setFont(new Font("Serif", Font.PLAIN, 20));
		panel1.add(label2);
		secondTextField.setPreferredSize(new Dimension(200,30)); //add second text field to panel
		panel1.add(secondTextField);
		//panel 2 displays the combo box for operation selection and the compute button 
		JPanel panel2 = new JPanel();
		panel2.setPreferredSize(new Dimension(500,10));
		comboBox.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		computeButton.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		panel2.add(comboBox);
		panel2.add(computeButton);
		//panel 3 display the text field where the result is displayed
		JPanel panel3 = new JPanel();
		JLabel label3 = new JLabel("Result         "); //label3 indicates that the result is displayed here
		label3.setFont(new Font("Serif", Font.PLAIN, 20));
		panel3.add(label3);
		panel3.setLayout(new FlowLayout());
		thirdTextField.setPreferredSize(new Dimension(200,30));
		panel3.add(thirdTextField);
		
		this.setTitle("Polynomial Calculator"); //set title of application to polynomial calculator
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(550,300); //set dimensions for frame
		JPanel generalPanel = new JPanel();
		//add all panels toghether and place the general panel on the frame
		generalPanel.add(panel0);
		generalPanel.add(panel1);
		generalPanel.add(panel2);
		generalPanel.add(panel3);
		generalPanel.setLayout(new BoxLayout(generalPanel, BoxLayout.Y_AXIS));
		this.setContentPane(generalPanel);
	}
	
	public String getFirstString() {
		
		return firstTextField.getText();
	}
	
	public String getSecondString() {
		
		return secondTextField.getText();
	}
	
	public void setResult(String result) {
		
		thirdTextField.setText(result);
	}
	
	public int getComboBoxIndex() {
		
		return comboBox.getSelectedIndex();
	}
	
	
	void showError(String errMessage) {
		
		JOptionPane.showMessageDialog(this, errMessage); //display a message dialog that states that an error happened 
	}
	
	void addComputeListener(ActionListener buttonPressed) {
		
		computeButton.addActionListener(buttonPressed);
	}
}
