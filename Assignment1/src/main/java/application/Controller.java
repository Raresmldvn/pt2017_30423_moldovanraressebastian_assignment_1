package application;

import java.awt.event.*;
import models.Polynomial;
public class Controller {


	private Operation thisOperation = new Operation();
	private View thisInterface = new View(thisOperation);
	
	public Controller(View myInterface, Operation myOperation) {
		this.thisInterface = myInterface;
		this.thisOperation = myOperation;
		thisInterface.addComputeListener(new ComputeListener());
	}
	
	private class ComputeListener implements ActionListener {
			
		public void actionPerformed(ActionEvent E){
			
			String firstInput = new String();
			try{ //put everything in a try block so that any type of exception suspends computations
			firstInput = thisInterface.getFirstString();
			isCorrect(firstInput);
			int comboBoxIndex = thisInterface.getComboBoxIndex();
			String secondInput = new String();
			if(comboBoxIndex<4) { //only if we need second polynomial, we construct it.
				
				    secondInput = thisInterface.getSecondString(); 
					isCorrect(secondInput);
				
			}
			Polynomial firstPolynomial = thisOperation.constructPolynomial(firstInput);
			Polynomial secondPolynomial;
			Polynomial result;
			String outputString = new String("");
			
			switch(comboBoxIndex){
			case 1: { //case of substraction
				secondPolynomial =  thisOperation.constructPolynomial(secondInput);
				result = (new Operation()).substractPolynomials(firstPolynomial, secondPolynomial);
				outputString = (new Operation()).representPolynomial(result);
				break;
			}
			case 2: { //case of multiplication
				secondPolynomial = thisOperation.constructPolynomial(secondInput);
				result = thisOperation.multiplyPolynomials(firstPolynomial, secondPolynomial);
				outputString = thisOperation.representPolynomial(result);
				break;
			}
			case 3: { //case of division
				secondPolynomial = thisOperation.constructPolynomial(secondInput);
				if(firstPolynomial.getDegree()<secondPolynomial.getDegree()|| secondPolynomial.getMonomials().size()==0) //throw exception if degrees are not correct
					throw new Exception("Cannot perform division. Degrees not correct!"); 
			    Polynomial[] intermediateResult = thisOperation.dividePolynomials(firstPolynomial, secondPolynomial);
			    outputString = "Q[X]= " + thisOperation.representPolynomial(intermediateResult[0]) + 
			    		"  R[X]= " + thisOperation.representPolynomial(intermediateResult[1]); 
				break;
			}
			case 4: { //case of differentiation
				result = thisOperation.differentiatiePolynomial(firstPolynomial);
				outputString = thisOperation.representPolynomial(result);
				break;
			}
			case 5: { //case of integration
				result = thisOperation.integratePolynomial(firstPolynomial);
				outputString = thisOperation.representPolynomial(result);
				break;
			}
			default: { //case of addition(default is index 0)
				secondPolynomial = thisOperation.constructPolynomial(secondInput);
			    result = thisOperation.addPolynomials(firstPolynomial,secondPolynomial);
				outputString = thisOperation.representPolynomial(result);
			}
			}
			thisInterface.setResult(outputString); //output string to the last text field using method setResult 
			}catch(Exception exc){
				thisInterface.showError(exc.getMessage()); //exceptions signaled with a new message box
			}
		
		}
		
		//method that checks, at a first glance, if characters are introduced correctly
		private void isCorrect(String input) throws Exception {
			
			for(int i=0;i<input.length();i++)
				if("0123456789^x+-".contains(Character.toString(input.charAt(i)))==false) //if not only valid characters are introduced
					throw new Exception("You provided some incorrect characters!");
		}
	}
}