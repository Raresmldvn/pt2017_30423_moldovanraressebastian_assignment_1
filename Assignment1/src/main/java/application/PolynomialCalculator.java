package application;


public class PolynomialCalculator {

	public static void main(String args[]){
		
		Operation myOperation = new Operation();
		View myInterface = new View(myOperation);
		Controller myController = new Controller(myInterface, myOperation);
		myInterface.setVisible(true);
	}
}
