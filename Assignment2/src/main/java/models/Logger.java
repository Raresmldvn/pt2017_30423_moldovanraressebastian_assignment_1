package models;

import java.util.concurrent.TimeUnit;

public class Logger {
	private String message;
	private long currentRefference;
	
	public Logger(){
		this.message="";
	}
	
	public void setTimeReference(long ref) {
		
		this.currentRefference = ref; //reference time is set at the beginning of the simulation with the current system time
	}
	
	public void startMessage() {
		
		this.message = this.message + "SIMULATION STARTED!" + "\n";
	}
	
	public void stopMessage() {
		
		this.message = this.message + "SIMULATION STOPPED!";
	}
	public void addClientMessage(Client C,int service, Queue Q){ //standard message for when adding clients
		
		this.message = this.message  + (TimeUnit.SECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS) - currentRefference) +
				": Client " + Integer.toString(C.getID()) + 
				" has service time: " + service  + " seconds and chose " + Q.printName() + "\n";	
	}
	
	public void deleteClientMessage(Client C, Queue Q){ //standard message for when deleting clients
		
		this.message = this.message + (TimeUnit.SECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS) - currentRefference) + ": Client " + Integer.toString(C.getID()) + " left " + Q.printName() + "\n";
	}
	
	public String getMessage(){
		
		return this.message; //get message so it can be displayed
	}

}
