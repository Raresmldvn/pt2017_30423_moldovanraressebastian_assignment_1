package models;


public class Client {
		
	private int cID;
	private int arrivalTime;
	private int serviceTime;
	private int totalTime = 0;
	
	public Client(int cID, int arr, int ser){
		
		this.cID = cID;
		this.arrivalTime = arr;
		this.serviceTime = ser;
	}
	
	public int getID() {
		return cID;
	}
	
	public int getArrival(){
		
		return arrivalTime;
	}
	
	
	public int getService(){
		
		return serviceTime;
	}
	
	public void setService(int newService){
		
		this.serviceTime = newService;
	}
	
	public int getTotalTime() {
		
		return totalTime;
	}
	
	public void setTotalTime(int t) {
		
		this.totalTime = this.totalTime + t;
	}
	
}
