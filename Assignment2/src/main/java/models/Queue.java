package models;

import java.util.*;

public class Queue extends Thread {
	
	private String queueName;
	private Vector<Client> clients;
	private Logger myLog;
	private int averageFinalTime = 0;
	private int totalNrOfClients = 0;
	
	public Queue(String name){
		this.queueName = name;
		this.clients = new Vector<Client>();
	}
	
	public void setLogger(Logger thisLogger){
		
		this.myLog = thisLogger; //set the logger of events 

	}
	
	public synchronized void addClient(Client currentClient){
		
		notifyAll(); //notify the other methods who are waiting(ex: the delete method)
		clients.add(currentClient); //add client to the vector
	}
	
	private synchronized void deleteClient() throws InterruptedException{
		
		while(clients.size()==0){
			wait(); //wait until the queue has at least one client
		}
		if(myLog.getMessage().contains("SIMULATION STOPPED!")==false){ //check if the simulation ended using the logger
			myLog.deleteClientMessage(clients.get(0), this); //add delete message to the logger
			averageFinalTime += clients.get(0).getTotalTime(); //increase average waiting time with the time of the leaving client
			totalNrOfClients ++; //increase number of clients who left
			clients.remove(0); //remove the first client in the queue
			notifyAll();
		}
	}
	
	public int getTotalServiceTime(){
		int totalServiceTime = 0;
		for(Client currentClient: clients){
			
			totalServiceTime+=currentClient.getService();
		}
		return totalServiceTime;
	}
	
	public float getFinalAverage() {
		
		return (float)averageFinalTime/totalNrOfClients;
	}
	public String printName(){
		
		return queueName;
	}
	
	public int getSize(){
		
		return clients.size();
	}
	
	public Vector<Client> getListOfClients(){
		
		return clients;
	}
	
	private synchronized void waitForValid() {
		
		try{
			while(clients.size()==0){ //wait for at least one client to show up
				wait();
			}
		}catch(InterruptedException e) {}
	}
	
	public void run(){
		while(true){
			try{
			waitForValid(); //wait for client to show up
			Thread.sleep(clients.get(0).getService()*1000); //sleep for the required number of seconds(the service time - seconds)
			deleteClient(); //delete first client
			}catch(InterruptedException exc) {
				System.out.println(exc.getMessage());
			}
		}
	}
	
	public synchronized String representQueue(){
		
		String representation="";
		for(Client currentClient: clients){
			
			representation = representation + "C" + Integer.toString(currentClient.getID()) + "  "; //representation of type C1 C2 ... Cn
		}
		return representation;
	}
	
	}
