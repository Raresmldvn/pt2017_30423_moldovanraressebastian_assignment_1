package applications;

import models.Store;
import java.awt.event.*;
import javax.swing.*;

public class Controller {

	View view;
	
	public Controller(View oneView){
		
		this.view = oneView;
		view.addStartListener(new StartListener());
		view.addResetListener(new ResetListener());
	}
	
	private class StartListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.addFinalPanel(view.getNrOfQueues());
			view.getStore().setIntervals(view.getMinArrival(), view.getMaxArrival(), view.getMinService(), view.getMaxService(), view.getInterval());
			view.getStore().start();
			new Thread(view).start();
		}
	}
	
	private class ResetListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			
		View auxiliary = new View();
		view.dispose();
		auxiliary.setVisible(true);
		view = auxiliary;
		Controller C = new Controller(view);
		}
	}
}
