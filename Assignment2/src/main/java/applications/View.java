package applications;

import models.Logger;
import models.Store;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class View extends JFrame implements Runnable {
	
	private JTextField minA = new JTextField(10);
	private JTextField maxA = new JTextField(10);
	private JTextField minS = new JTextField(10);
	private JTextField maxS = new JTextField(10);
	private JTextField interval = new JTextField(10);
	private JTextField nrQueues = new JTextField(10);
	private JButton startSimulation = new JButton("Start");
	private JButton resetSimulation = new JButton("Reset");
	private JPanel finalPanel = new JPanel();
	private JTextField[] queueText;
	private JLabel[] queueServices;
	private JTextField avg;
	private JTextField peak;
	private JTextField empty;
	private JTextArea log = new JTextArea();
	
	private Store myStore;
	private Logger myLogger = new Logger();
	
	public View() {
		
		//myStore.run();
		JPanel titlePanel = new JPanel();
		JLabel title = new JLabel("Queue Simulator");
		title.setFont(new Font("Serif", Font.PLAIN, 60));
		titlePanel.add(title);
		
		JPanel decisionsPanel = new JPanel();
		JLabel minArrivalLabel = new JLabel("Minimum Arrival");
		JLabel maxArrivalLabel = new JLabel("Maximum Arrival");
		JLabel minServiceTimeLabel = new JLabel("Minum Service");
		JLabel maxServiceTimeLabel = new JLabel("Maximum Service");
		JLabel simulationInterval = new JLabel("Interval");
		decisionsPanel.add(minArrivalLabel);
		minA.setPreferredSize(new Dimension(10,20));
		maxA.setPreferredSize(new Dimension(10,20));
		minS.setPreferredSize(new Dimension(10,20));
		maxS.setPreferredSize(new Dimension(10,20));
		interval.setPreferredSize(new Dimension(10,20));
		decisionsPanel.add(minArrivalLabel);
		decisionsPanel.add(minA);
		decisionsPanel.add(maxArrivalLabel);
		decisionsPanel.add(maxA);
		decisionsPanel.add(minServiceTimeLabel);
		decisionsPanel.add(minS);
		decisionsPanel.add(maxServiceTimeLabel);
		decisionsPanel.add(maxS);
		decisionsPanel.add(simulationInterval);
		decisionsPanel.add(interval);
		
		JPanel startPanel = new JPanel();
		JLabel descriptionLabel = new JLabel("Choose number of queues and press Start to start simulation");
		startPanel.add(descriptionLabel);
		startPanel.add(nrQueues);
		startPanel.add(startSimulation);
		startPanel.add(resetSimulation);
		
		
		//JPanel finalPanel = new JPanel();
		finalPanel.add(titlePanel);
		finalPanel.add(decisionsPanel);
		finalPanel.add(startPanel);
		this.add(finalPanel);
		//if(queuePanel.getComponents().length>0)
			//finalPanel.add(queuePanel);
		//this.setLayout
		//this.setLayout(new GridLayout());
		this.setSize(1100, 600);
	}
	
	public int getNrOfQueues() {
		
		return Integer.parseInt(nrQueues.getText());
	}
	
	public int getMinArrival() {
		
		return Integer.parseInt(minA.getText());
	}
	
	public int getMaxArrival() {
		
		return Integer.parseInt(maxA.getText());
	}
	
	public int getMinService() {
		
		return Integer.parseInt(minS.getText());
	}
	
	public int getMaxService() {
		
		return Integer.parseInt(maxS.getText());
	}
	
	public int getInterval() {
		
		return Integer.parseInt(interval.getText());
	}
	
	public void addStartListener(ActionListener buttonPressed){
		
		startSimulation.addActionListener(buttonPressed);
	}
	
	public void addResetListener(ActionListener buttonPressed){
		
		resetSimulation.addActionListener(buttonPressed);
	}
	
	public void addFinalPanel(int nr){ //function that dynamically paints on the final panel the necessary information for the simulation
		
		JPanel newPanel = new JPanel();
		queueText = new JTextField[nr]; //initialize the array of text fields
		queueServices = new JLabel[nr]; //initialize the array of labels for the service presentation
		myStore = new Store(nr);
		myStore.setLogger(myLogger);
		for(int i=0; i<nr; i++){
			newPanel.add(new JLabel("Queue" + i));
			queueText[i] = new JTextField(70); //create text field for queue
			queueServices[i] = new JLabel(); //create label for queue
			newPanel.add(queueText[i]);
			newPanel.add(queueServices[i]);
		}
		newPanel.setLayout(new BoxLayout(newPanel, BoxLayout.PAGE_AXIS));
		log = new JTextArea(10,80); //create logger
		JScrollPane eventsArea = new JScrollPane(log);
		JPanel newPanel2 = new JPanel();
		newPanel2.add(new JLabel("LOG OF EVENTS"));
		newPanel2.add(eventsArea);
		JPanel newPanel3 = new JPanel();
		//create components for the statistical output
		avg = new JTextField(10);
		peak = new JTextField(10);
		empty = new JTextField(10);
		newPanel3.add(new JLabel("Average Waiting Time"));
		newPanel3.add(avg);
		newPanel3.add(new JLabel("Peak Second"));
		newPanel3.add(peak);
		newPanel3.add(new JLabel("Total Empty Queue Time"));
		newPanel3.add(empty);
		//add everything to the final panel
		finalPanel.add(newPanel);
		finalPanel.add(newPanel2);
		finalPanel.add(newPanel3);
		this.add(finalPanel); //add final panel tot the frame
		setVisible(true); //repaint panel
	}

	
	public synchronized Store getStore() {
		
		return myStore;
	}
	
	private synchronized void updateTextField(String update, int i){
		
		JTextField currentTextField = queueText[i];
		currentTextField.setText(myStore.getQueues()[i].representQueue());
	}
	
	private synchronized void updateQueues(){
		
		for(int i=0; i<queueText.length; i++){

			updateTextField(myStore.getQueues()[i].representQueue(), i); //set text fields
			queueServices[i].setText("Total Service Time:" + Integer.toString(myStore.getQueues()[i].getTotalServiceTime())); //set labels
		}
	}
	
	public synchronized void updateEvents(){
		
		log.setText(myLogger.getMessage()); //set logger
	}

	
	public void run() {
		
		boolean firstTime=true;
		while(true){
		updateQueues();
		updateEvents();
		if(myLogger.getMessage().contains("STOPPED!")==true&&firstTime==true){ //if logger contains the stop message, meaning that the simulation is over
			
			avg.setText(Float.toString(myStore.getTotalAverageTime())); //set average
			peak.setText(Integer.toString(myStore.getPeakSecond())); //set peak second
			empty.setText(Integer.toString(myStore.getEmptyQueueTime())); //set empty queue total time
			firstTime=false; //once set, never set them again
		}
		}
	}
}
